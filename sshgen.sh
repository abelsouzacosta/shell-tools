#!/bin/bash

generate_key() {
	ssh-keygen -t ed25519 -C "$1"
}

print_help() {
	echo "
		Usage: $(basename 0) [-h]
		-h: Display the help message
	"
}

while getopts ":h" opt; do
	case $opt in
		h)
			print_help
			exit 0
			;;
		\?)
			echo "Invalid option: $OPTARG" 1>&2
			print_help
			exit 1
			;;
	esac
done
shift $((OPTIND -1))

read -p "Comment for the ssh key: " comment

machine_hostname=$(hostname)

if [[ -n "$comment" ]]; then
	generate_key "$comment"
else
	generate_key "$machine_hostname"
fi

echo "Evaluating ssh-agent..."
eval "$(ssh-agent -s)"
echo "Adding ssh to the private ssh-agent"
ssh-add ~/.ssh/id_ed25519

if which xclip >/dev/null 2>&1; then
       echo "Copying to the transfer area"
       cat ~/.ssh/*.pub | xclip -sel clip
else
	echo "You dont have xclip installed copy the output below"
	cat ~/.ssh/*.pub
fi
