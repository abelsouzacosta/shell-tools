#!/bin/bash

validate() {
	# use sed to capture the valid characters input
	valid_characters="$(echo $1 | sed -e 's/[^[:alnum:]]//g')"
	# test if the valid characters are equal to the parameter given
	if [[ "$valid_characters" == "$1" ]]; then
		return 0
	else
		return 1
	fi
}

print_help() {
	echo "
		Will validate if the input are only alphanumeric characters
		Usage: $0 [-h]
			-h: Show this message
	"
}

while getopts ":h" opt; do
	case $opt in
		h)
			print_help
			exit 0
			;;
		\?)
			echo "Invalid option: $OPTARG" 1>&2
			print_help
			exit 1
			;;
	esac
done
shift $((OPTIND - 1))

message="$1"

# read the input
read -p "$message: " input

# tests the input against function
if ! validate "$input"; then
	echo "Please enter inly letters and numbers." >&2
	exit 1
else
	echo "Input is valid"
fi

exit 0
