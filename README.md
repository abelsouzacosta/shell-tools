# Shell Tools

Useful terminal tools

1. `lncount`: count all the lines inside of a plain text (txt) file, allows to read all lines or non white lines
2. `sshgen`: create and add ssh keys to the agent, and allows you to copy it
3. `alpnonly`: checks if an input given is an valid alphanumeric
4. `validint`: checks if the input is a valid integer
