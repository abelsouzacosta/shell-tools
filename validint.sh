#!/bin/bash

print_help() {
	echo "
		Checks if the input is an valid integer
		Usage $0 [-lmh] value
			-l: lower number, the input should be higher than this number
			-m: max number, the input should be lower than this number
			-h: show this message help
	"
}

options=":hl:m:"

while getopts $options opt; do
	case $opt in
		l) 
			min_value="$OPTARG"
			;;
		m)
			max_value="$OPTARG"
			;;
		h)
			print_help
			exit 0
			;;
		\?) 
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
	esac
done

shift $((OPTIND - 1))

input="$1"

is_valid_integer() {
	local input=$1
	local min=$2
	local max=$3

	# checks if the input is really a number
	if [[ $input =~ ^[0-9]+$ ]]; then
		# checks if there's a lower value
		# and compares to the input
		if [[ -n $min && $input -lt $min ]]; then
			return 1 # number out of range
		fi

		# checks if there's a higher value
		# and compares to the input
		if [[ -n $max && $input -gt $max ]]; then
			return 1 # number out of range
		fi
		return 0 # return true if the input is in the range
	else
		return 1 # not a number
	fi
}

if is_valid_integer "$input" "$min_value" "$max_value"; then
	echo "Valid input"
	exit 0
else
	echo "Invalid input"
	exit 1
fi
