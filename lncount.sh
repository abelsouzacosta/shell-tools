#!/bin/bash

display_help() {
	echo "$help_message"
}

count_all_lines() {
	local filename="$1"
	local count=0
	while read -r line; do
		count=$((count + 1))
	done < "$filename"
	echo "The number of lines in the file '$filename' is $count"
}

count_non_whitelines() {
	local filename="$1"
	local count=0
	while read -r line; do
		if [[ -n "${line// }" ]]; then # Check if the line is not empty
			count=$((count + 1))
		fi
	done < "$filename"
	echo "The number of lines in the file '$filename' is $count"
}

# process the options provided in the command
options="hwa" # valid options

while getopts $options arg; do
	case $arg in
		h)
			display_help
			exit 0
			;;
		w)
			count_non_whitelines "$2"
			;;
		a)
			count_all_lines "$2"
			;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			exit 1
			;;
	esac
done

# check if a filename is provided to the script
if [[ -z "$2" ]]; then
	echo "Error: Missing filename argument" >&2
	exit 1
fi
